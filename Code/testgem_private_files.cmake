
set(FILES
    Source/TestGemModuleInterface.h
    Source/Clients/TestGemSystemComponent.cpp
    Source/Clients/TestGemSystemComponent.h
    Source/Clients/MyComponent.cpp
    Source/Clients/MyComponent.h
    Source/Clients/RaycastTest.cpp
    Source/Clients/RaycastTest.h
    Source/Clients/HeadBob.cpp
    Source/Clients/HeadBob.h
    Source/Clients/PlayerControllerComponent.cpp
    Source/Clients/PlayerControllerComponent.h
    Source/Clients/CameraShake.cpp
    Source/Clients/CameraShake.h
    Source/Clients/Grab.cpp
    Source/Clients/Grab.h
)

